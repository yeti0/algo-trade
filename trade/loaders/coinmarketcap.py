import requests
import json
import pandas as pd

""" https://coinmarketcap.com/api/documentation/v1/ """

class Coinmarketcap:
    def __init__(self, api_key):
        self.api_key = api_key

        self.__get_latest()

    def __get_latest(self):
        root_url = "https://pro-api.coinmarketcap.com"
        headers = {'Accept': 'application/json',
                   'X-CMC_PRO_API_KEY': self.api_key}
        jsond = {"start": 1, "limit": 5000, "convert": "USD"}
        url = root_url + '/v1/cryptocurrency/listings/latest'
        data = json.loads(requests.get(url, headers=headers, data=jsond).text)
        self.df = pd.json_normalize(data["data"])
# {
#     "id": 2416,
#     "name": "THETA",
#     "symbol": "THETA",
#     "slug": "theta",
#     "num_market_pairs": 42,
#     "date_added": "2018-01-17T00:00:00.000Z",
#     "tags": [
#         "media",
#         "collectibles-nfts",
#         "content-creation",
#         "video"
#     ],
#     "max_supply": null,
#     "circulating_supply": 1000000000,
#     "total_supply": 1000000000,
#     "platform": null,
#     "cmc_rank": 34,
#     "last_updated": "2020-12-14T14:30:05.000Z",
#     "quote": {
#         "USD": {
#             "price": 0.76161208058671,
#             "volume_24h": 35180879.88255338,
#             "percent_change_1h": 3.31243642,
#             "percent_change_24h": 6.97760417,
#             "percent_change_7d": 10.51668687,
#             "market_cap": 761612080.5867101,
#             "last_updated": "2020-12-14T14:30:05.000Z"
#         }
#     }
# },
