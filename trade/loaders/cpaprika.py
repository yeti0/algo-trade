from coinpaprika import client as Coinpaprika

""" https://github.com/s0h3ck/coinpaprika-api-python-client/blob/master/examples/examples.py """

client = Coinpaprika.Client()

# List coins
print(client.markets("btc-bitcoin", quotes="USD"))
# client.coin("btc-bitcoin")
