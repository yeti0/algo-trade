import requests
import json
import pandas as pd

import datetime as dt

"""  https://www.coingecko.com/en/api#explore-api """


class Coingecko:
    def __init__(self, coin, vs_currency, days):
        self.coin = coin
        self.days = days
        self.vs_currency = vs_currency
        self.root_url = "https://api.coingecko.com/api/v3/coins/"
        self.__get_latest()

    # ['id',
    # 'symbol',
    # 'name',
    # 'image',
    # 'current_price',
    # 'market_cap',
    # 'market_cap_rank',
    # 'fully_diluted_valuation',
    # 'total_volume',
    # 'high_24h',
    # 'low_24h',
    # 'price_change_24h',
    # 'price_change_percentage_24h',
    # 'market_cap_change_24h',
    # 'market_cap_change_percentage_24h',
    # 'circulating_supply',
    # 'total_supply',
    # 'max_supply',
    # 'ath',
    # 'ath_change_percentage',
    # 'ath_date',
    # 'atl',
    # 'atl_change_percentage',
    # 'atl_date',
    # 'roi',
    # 'last_updated']
    def __get_latest(self):
        pd.set_option('display.float_format', '{:.2f}'.format)
        # url = self.root_url + self.coin + "/market_chart?vs_currency=" + self.vs_currency + "&days=" + self.days
        url = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=bitcoin"
        data = json.loads(requests.get(url).text)
        self.df = pd.json_normalize(data)
        # self.df = pd.DataFrame({
        #     'Dates': [x[0] for x in data['last_updated']],
        #     'Total_Marketcap': [x[1] for x in data['market_caps']],
        #     'Daily_volume': [x[1] for x in data['total_volumes']]})
        # self.df.Dates = [dt.datetime.fromtimestamp(x / 1000.0) for x in self.df.Dates]
        columns = ['id', 'symbol', 'name', 'image', 'current_price', 'market_cap', 'market_cap_rank', 'fully_diluted_valuation', 'total_volume', 'high_24h', 'low_24h', 'price_change_24h', 'price_change_percentage_24h', 'market_cap_change_24h', 'market_cap_change_percentage_24h', 'circulating_supply', 'total_supply', 'max_supply', 'ath', 'ath_change_percentage', 'ath_date', 'atl', 'atl_change_percentage', 'atl_date', 'roi', 'last_updated']
        self.df.columns = columns
        self.df = self.df[columns]
        self.df.to_csv (r'coingecko.csv', index = False, header=False,mode='a')
