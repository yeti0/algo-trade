import requests
import json
import pandas as pd
import datetime as dt
import numpy as np
import matplotlib.pyplot as plt

""" https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md """
class Binance:
    def __init__(self, symbol, interval):
        self.symbol = symbol
        self.interval = interval
        self.root_url = "https://api.binance.com/api/v1/klines"
        self.__get_bars()

    def __get_bars(self):
        pd.set_option('display.float_format','{:.2f}'.format)
        url = self.root_url + '?symbol=' + self.symbol + '&interval=' + self.interval

        data = json.loads(requests.get(url).text)
        self.df = pd.DataFrame(data)
        self.df.columns = ['open_time',
                           'o', 'h', 'l', 'c', 'v',
                           'close_time', 'qav', 'num_trades',
                           'taker_base_vol', 'taker_quote_vol', 'ignore']


        self.df.close_time = [dt.datetime.fromtimestamp(x / 1000.0) for x in self.df.close_time]
        self.df = self.df[['close_time', 'o', 'h', 'l', 'c']]
        # self.df = self.df.sort_values('close_time')
        #
        # self.df['date_time_int'] = self.df.close_time.astype(np.int64)
        # fig, ax = plt.subplots(figsize=(10,6))
        # a = self.df.loc[self.df['anomaly1'] == 1, ['date_time_int', 'c']] #anomaly
        # #
        # ax.plot(df['date_time_int'], df['c'], color='blue', label='Normal')
        # ax.scatter(a['date_time_int'],a['c'], color='red', label='Anomaly')
        # plt.xlabel('Date Time Integer')
        # plt.ylabel('price in USD')
        # plt.legend()
        # plt.show()