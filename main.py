#!/usr/bin/env python
from trade.loaders import binance
from trade.loaders import coinmarketcap
from trade.loaders import coingecko
import os


def main():
    # print("BINANCE")
    # bin = binance.Binance(symbol="ETHUSDT",
    #                       interval="1m")
    # print(bin.df)

    # print("COINMARKETCAP")
    # cmc_pro_api_key = os.getenv('CMC_PRO_API_KEY', "cmc_pro_api_key")
    # coin = coinmarketcap.Coinmarketcap(api_key=cmc_pro_api_key)
    # print(coin.df.columns.tolist())
    # print(coin.df)
#
    print("COINGECKO")
    cgecko = coingecko.Coingecko(coin="bitcoin",
                                 vs_currency="usd",
                                 days="200")
    print(cgecko.df)
#
#
if __name__ == '__main__':
    main()
